﻿module Adventure.Map

open Adventure.Utilities

type public Tile = | Player | Character | Wall | Floor

(* Map Validation *)
let linesAreSameLength (lines: string list) =
    let length = lines.[0].Length
    (true, lines) ||> List.fold (fun s v -> s && v.Length = length)
let charIs key test = test = key
let charIsHash = charIs '#'
let lineIsWall line = line |> Seq.map charIsHash |> Seq.reduce calcAnd
let topAndBottomAreWalls (lines: string list) = (lines.[0] |> lineIsWall) && (lines.[lines.Length-1] |> lineIsWall)
let leftAndRightAreWalls (lines: string list) =
    lines
    |> List.map (fun line -> (charIsHash line.[0]) && (charIsHash line.[line.Length-1]))
    |> List.reduce calcAnd
    
let mapIsValid lines =
    (linesAreSameLength lines) && (topAndBottomAreWalls lines) && (leftAndRightAreWalls lines)

(* Map Creation *)
let mapToTile inp =
    match inp with
    | '.' -> Floor
    | '#' -> Wall
    | 'p' -> Player
    | 'c' -> Character
    |  _  -> Floor
let mapLine line = line |> Seq.map mapToTile |> Array.ofSeq
let parseLines lines = lines |> List.map mapLine |> Array.ofList

(* Map Display *)
let mapToChar tile =
    match tile with
    | Floor -> '.'
    | Wall -> '#'
    | Player -> 'p'
    | Character -> 'c'
let mapLineToChars line = line |> Array.map mapToChar |> System.String.Concat
let mapBoardToDisplay board = board |> Array.map mapLineToChars |> Array.reduce (fun x y -> x + "\n" + y)

(* Default action if invalid map will be to generate a map. For now, we'll make a default, simple map *)
let defaultMap =
    let thing = [ "###"; "#.#"; "###" ]
    parseLines thing

    