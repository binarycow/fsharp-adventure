﻿module Adventure.Abilities

open Adventure.Characters
module Species =
    open Adventure.Species
    (* Species Abilities
    We might make these available to other species, 
    so they're in their own functions for now.
    *)
    let enrage caster = changeAttack caster.SpecialAbilityGain caster
    let sneak caster = changeSneak caster.SpecialAbilityGain caster
    let rally caster = changeDefense caster.SpecialAbilityGain caster
    let specialAbilityCost caster = changeCurrentHealth caster.SpecialAbilityCost caster
    let specialAbility caster =
        let newState =
            match caster.Species with
            | Orc -> enrage caster 
            | Goblin -> sneak caster
            | Human -> rally caster
        specialAbilityCost newState
module Specialization =
    open Adventure.Specializations
    let x = Wizard