﻿module Adventure.Utilities 
let square x = x * x
let calcDistance x1 y1 x2 y2 =
    let dX = x2 - x1 |> square
    let dY = y2 - y1 |> square
    let root = dX + dY |> float |> sqrt
    int root
let calcAnd x y = x && y