﻿module Adventure.Characters

open Adventure.Specializations
open Adventure.Species
    
type public Character =
    {
        Name: string
        
        Specialization: Specialization
        Species: Species
        
        Attack: int
        Defense: int
        
        Reach: int
        
        MaxHealth: int
        CurrentHealth: int
        
        Sneak: int
        
        Position: int * int
        
        SpecialAbilityCost: int
        SpecialAbilityGain: int
        
        Player: bool
    }

let changeMaxHealth n target = {target with MaxHealth = target.MaxHealth + n} 
let changeCurrentHealth n target = {target with CurrentHealth = target.CurrentHealth + n}
let changeAttack n target = {target with Attack = target.Attack + n} 
let changeDefense n target = {target with Defense = target.Defense + n}
let changeSneak n target = {target with Sneak = target.Sneak + n}
let attack attacker defender =
    let attackDamage = attacker.Attack - defender.Defense
    changeCurrentHealth (max 0 attackDamage) defender
let changePosition target x y = {target with Position = (x, y)}

let isSameCharacter character1 character2 = character1.Name = character2.Name
let isPlayer character = character.Player
    